import cgi
import cgitb
cgitb.enable()

form = cgi.FieldStorage()
print("Content-type: text/html; charset=utf-8\n")


html = """<!DOCTYPE html>
<head>
    <title>MPG Scrapping</title>
</head>
<body>
    <form action="/index.py" method="post">
        Le code de votre ligue MPG : <input type="text" name="league" value="LW6BN4AH" /><br>
        Le token d'autorisation : <input type="text" name="auth_token" value="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzQ5NDM0NCIsImNoZWNrIjoiMmE4MmQxZTZlMjQzMmZjYSIsImlhdCI6MTU2OTM0Mzg0Nn0.SPCUXOF0Y8kgU5pIw5Fp6DmnESqw91DU6R0UeS5AsxI" /><br>
        <input type="submit" name="send" value="Afficher les blessés de toute la ligue !"><br>
    </form> 
</body>
</html>
"""
print(html)


if form.getvalue("league") and form.getvalue("auth_token"):
    import display_json

    display_json.scrap_mpg_injured(form.getvalue("league"), form.getvalue("auth_token"))
