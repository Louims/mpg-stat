import requests
import json


def scrap_mpg_injured(league_id, auth_token):
    # CONFIGURATION

    # league_id = 'LW6BN4AH'
    # league_id = 'LW6BN464'
    # auth_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzE5NzMzNTQiLCJj \
    # aGVjayI6Ijc5OTUzYzJkZTljNWM4NTUiLCJpYXQiOjE1NzY3NjQ2ODN9.OQnH1hyhvnwcpc-o78W1_4EoLtBBgyxgzruk3QtDb3I"

    print("division 1 Auth ID :<br>")
    print("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzQ5NDM0NCIsImNoZWNrIjoiMmE4MmQxZTZlMjQzMmZjYSIsImlhdCI6MTU2OTM0Mzg0Nn0.SPCUXOF0Y8kgU5pIw5Fp6DmnESqw91DU6R0UeS5AsxI<br>")
    print("ligue : LW6BN4AH<br><br>")

    print("division 2 Auth ID :<br>")
    print("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Im1wZ191c2VyXzE5NzMzNTQiLCJjaGVjayI6Ijc5OTUzYzJkZTljNWM4NTUiLCJpYXQiOjE1NzY3NjQ2ODN9.OQnH1hyhvnwcpc-o78W1_4EoLtBBgyxgzruk3QtDb3I<br>")
    print("ligue : LW6BN464")

    # GET ALL USERS OF THE LEAGUE
    mpg_user_ids = {}
    ranking_teams_json = requests.get('https://api.monpetitgazon.com/league/'+league_id+'/ranking')
    ranking_teams = json.loads(ranking_teams_json.text)

    # GET ALL TEAMS OF USERS OF THE LEAGUE
    mpg_teams = {}
    for team in ranking_teams["teams"]:
        r = requests.get('https://api.monpetitgazon.com/stats/championship/1?filter=team&teamId=' + team)
        mpg_teams[ranking_teams["teams"][team]["name"]] = json.loads(r.text)

    r2 = requests.get(
        'https://api.monpetitgazon.com/league/' + league_id + '/coach',
        '',
        headers={'Authorization': auth_token})

    pretty_json2 = json.loads(r2.text)

    # split array of teams : players / injured / uncertain
    all_blesses_or_pending = pretty_json2['data']['tds']
    injured_by_team = {}
    pending_by_team = {}



    for mpg_user_id, mpg_team in mpg_teams.items():
        injured_by_team[str(mpg_user_id)] = {}
        pending_by_team[str(mpg_user_id)] = {}
        for player in mpg_team:
            if player["id"] in all_blesses_or_pending and "injured" in all_blesses_or_pending[player["id"]]:
                injured_by_team[str(mpg_user_id)][(player["firstname"] or '') + " " + (player["lastname"] or '')] = '(' + str(player["quotation"]) + ')'
                mpg_teams[mpg_user_id].remove(player)
            if player["id"] in all_blesses_or_pending and "pending" in all_blesses_or_pending[player["id"]]:
                pending_by_team[str(mpg_user_id)][(player["firstname"] or '') + " " + (player["lastname"] or '')] = '(' + str(player["quotation"]) + ')'
                mpg_teams[mpg_user_id].remove(player)

    # Display injured and uncertain :
    import json2table
    build_direction = "LEFT_TO_RIGHT"
    table_attributes = {"style": "border:1px solid black"}
    print("blessés : <br>")
    print(json2table.convert(injured_by_team,
                             build_direction=build_direction,
                             table_attributes=table_attributes))
    print("Incertains : <br>")
    print(json2table.convert(pending_by_team,
                             build_direction=build_direction,
                             table_attributes=table_attributes))


    for mpg_user_id, mpg_team in mpg_teams.items():
        better_goal = 2.5
        goalkeeper = {}
        defs={}
        mil={}
        att={}
        for player in mpg_team:
            if player["position"] == 1:
                goalkeeper[player["id"]] = player
            if player["position"] == 2:
                defs[player["id"]] = player
            if player["position"] == 3:
                mil[player["id"]] = player
            if player["position"] == 4:
                att[player["id"]] = player

        # mpg_pronostic_team = {goalkeeper, defs, mil, att}

        print("Attaquants : <br>")
        print(json2table.convert(att,
                                 build_direction=build_direction,
                                 table_attributes=table_attributes))

                # = player["quotation"] \
                #     if player["quotation"] > better_goal and player["stats"]["percentageStarter"] > 5 \
                #     else better_goal






