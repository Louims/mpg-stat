#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from lxml import etree
import io
import sys

PY2 = sys.version_info[0] == 2
StringIO = io.BytesIO if PY2 else io.StringIO

# Caractères à ignorer (non conforme xml)
characters_to_ignore = "&"
# Activation des tests supplémentaires
check_section = False
check_bullet = False

def XmlValidate(xml_to_validate, line_number, csv_col_number):
    result = True
    # Test #1 : verification validité xml
    clean_xml_to_validate = ''.join(c for c in xml_to_validate if c not in characters_to_ignore)
    try:
        doc = etree.fromstring("<xml>" + clean_xml_to_validate + "</xml>")
    except etree.ParseError as err:
        print("  - Ligne #" + str(line_number) + " Parsing XML Error (formatage xml colonne #"+str(csv_col_number)+") : " + err.msg + "\n"+xml_to_validate)
        return False
    except:
        print("  - Ligne #" + str(line_number) + " Error (formatage xml colonne # "+str(csv_col_number)+") : " + "\n"+ xml_to_validate)
        return False

    # Test #2 : Sur chaque ligne, une seule taille de police
    # Dans chaque paragraphe, on doit avoir une seule dimension (ie une seule section)
    # ie <paragraph><section>sdsdsd</section></paragraph>
    if check_section:
        for paragraph in doc.findall('paragraph'):
            if len(paragraph.findall('section')) > 1:
                print("  - #" + str(line_number) + " Erreur une seule section autorisée par paragraphe (colonne "+str(csv_col_number)+")\n"+xml_to_validate)
                result = False
    # Test #3 : pas de puce dans les sections
    # ie sous <section><header> puces interdites
    # Recherche des puces puis de leur parent
    if check_bullet:
        found_puce = [element for element in doc.iter() if element.text is not None and '•' in element.text]
        for contenu_puce in found_puce:
            if contenu_puce.getparent().tag == 'header' or contenu_puce.getparent().tag == 'section':
                print("  - #" + str(line_number) + " Pas de puce en taille non normale (colonne "+str(csv_col_number)+")\n"+contenu_puce.text)
                result = False
    return result

# ------------ MAIN ---------------------
for file in os.listdir("."):
    if file.endswith(".csv"):
        with open(file,"r",encoding="utf8", errors='ignore') as fp: #ignore special characters producing errors
            print("\n* Vérification du fichier : "+file+"\n------------------------------------------------")
            nberrors = 0
            for cpt, line in enumerate(fp):
                tline = str.split(line,"\t")
                numcolonne = 0
                numligne = cpt + 1

                for strcolonne in tline:
                    numcolonne = numcolonne + 1
                    if strcolonne.startswith("<paragraph"): #Détection donnée xml par la présence de la balise paragph
                        if not XmlValidate(strcolonne, numligne, numcolonne):
                            nberrors += 1

            print("* "+str(numligne) + " lignes vérifiées ("+str(nberrors)+" erreur(s))")